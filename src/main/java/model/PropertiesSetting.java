package model;

import java.io.*;
import java.util.Objects;
import java.util.Properties;

import Exeptions.NotCreatedObjectExeption;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * Class which contains method for getting property value.
 */
public class PropertiesSetting {
    /**
     * Method which returns values from game.properties file.
     *
     * @param property property value needed to return
     * @return property value
     */
    final Logger logger = LogManager.getLogger(PropertiesSetting.class);
    final String Way = "game.properties";
    public final String getPropertyValue(final String property) {
        String propertyValue = "";
        Properties properties = new Properties();
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream(Way));
            propertyValue = properties.getProperty(property);
            if(Objects.isNull(propertyValue)){
                try {
                    throw new NotCreatedObjectExeption();
                } catch (NotCreatedObjectExeption e) {
                    e.showMessage();
                }
            }
        } catch (FileNotFoundException e) {
            logger.error("Property file not found!");
        } catch (IOException e) {
            logger.error("Problems with loading properties!");
        }
        return propertyValue;}
}
