package model;

import java.awt.Graphics;
import java.awt.Color;
import java.io.IOException;
import java.util.Objects;

/**
 * Class which represents all human paddle methods and variables.
 */
public class HumanPaddle {
    /**
     * Represent human paddle coordinates on Y axis.
     */
    private static double positionY;
    /**
     * Represent human paddle coordinates on X axis.
     */
    private int positionX;
    /**
     * Represent human paddle speed on Y axis.
     */
    private double ySpeed;
    /**
     * Represent like human paddle inertia.
     */
    private final double brakePower;
    /**
     * Represent human paddle upper acceleration.
     */
    private boolean upAcceleration;
    /**
     * Represent human paddle down acceleration.
     */
    private boolean downAcceleration;
    /**
     * Represent human paddle height.
     */
    private int widthRect;

    private int heightRect;
    /**
     * Variable which stands for double value of WIDTH_RECT variable.
     */
    private double doubleWidthRect;
    /**
     * Represents max speed of human paddle on Y axis.
     */
    private int maxSpeedY;

    private Object humanPaddle;

    /**
     * Constructor which initialize variables.
     */
    public HumanPaddle() throws IOException {
        upAcceleration = false;
        downAcceleration = false;
        widthRect = Constants.WIDTH_RECT;
        heightRect = Constants.HEIGHT_RECT;
        doubleWidthRect = widthRect * 2;
        positionY = Constants.HALF_HEIGHT_SCREEN - doubleWidthRect;
        maxSpeedY = Integer.valueOf(new PropertiesSetting().getPropertyValue("maxSpeedY"));
        ySpeed = maxSpeedY;
        positionX = widthRect;
        brakePower = Double.valueOf(new PropertiesSetting().getPropertyValue("brakePower"));

    }

    /**
     * Method with signature and body which stands for drawing human paddle.
     *
     * @param graphics object of Graphics type
     */
    public final void draw(final Graphics graphics) {
        graphics.setColor(Color.WHITE);
        graphics.fillRect(positionX, (int) positionY, widthRect, heightRect);
    }

    /**
     * Method with signature and body which stands for moving human paddle.
     */
    public final void move() {
        if (upAcceleration) {
                ySpeed = ySpeed - 2;
        } else if (downAcceleration) {
            ySpeed = ySpeed + 2;
        } else if (!upAcceleration && !downAcceleration) {
            ySpeed = ySpeed * brakePower;
        }
        if (ySpeed >= maxSpeedY) {
            ySpeed = maxSpeedY;
        } else if (ySpeed <= -maxSpeedY) {
            ySpeed = -maxSpeedY;
        }
        positionY = positionY + ySpeed;
        if (positionY < 0) {
            positionY = 0;
        }
        if (positionY > (Constants.HEIGHT_SCREEN - heightRect)) {
            positionY = (Constants.HEIGHT_SCREEN - heightRect);
        }
    }

    /**
     * Method ith signature and body
     * which stands for acceleration up of controlled by human paddle.
     *
     * @param input param which shows whether human paddle moves
     */
    public final void setUpAcceleration(final boolean input) {
        upAcceleration = input;
    }

    /**
     * Method ith signature and body
     * which stands for acceleration down of controlled by human paddle.
     *
     * @param input param which shows whether human paddle moves
     */
    public final void setDownAcceleration(final boolean input) {
        downAcceleration = input;
    }

    /**
     * Gag for unused method here.
     *
     * @return null
     */
    final double getX() {
        return new Double(String.valueOf(Objects.isNull(humanPaddle)));
    }

    /**
     * Getter for y coordinates of human paddle.
     *
     * @return y coordinate
     */
    static double getY() {
        return positionY;
    }
    /**
     * Getter for doubleWidthRect variable of human paddle.
     *
     * @return doubleWidthRect value
     */
    final double getDoubleWidthRect() {
        return doubleWidthRect;
    }
    /**
     * Gag for unused method here.
     */
    void checkPaddleCords() {
    }
}
