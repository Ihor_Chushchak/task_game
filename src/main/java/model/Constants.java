package model;

import Exeptions.NotFoundPropertyVariableExeption;

public class Constants {

    public static final int HEIGHT_RECT = Integer.valueOf(
            new PropertiesSetting().getPropertyValue("heightRect"));

    public static final int WIDTH_RECT = Integer.valueOf(
            new PropertiesSetting().getPropertyValue("widthRect"));

    public static final int RADIUS = Integer.valueOf(
            new PropertiesSetting().getPropertyValue("ballRadius"));

    public static final int WIDTH_SCREEN = Integer.valueOf(
            new PropertiesSetting().getPropertyValue("widthScreen"));

    public static final int HEIGHT_SCREEN = Integer.valueOf(
            new PropertiesSetting().getPropertyValue("heightScreen"));

    public static final int HALF_HEIGHT_SCREEN = HEIGHT_SCREEN / 2;

    public static final int HALF_RADIUS = RADIUS / 2;

    public static final int HALF_HEIGHT_RECT = HEIGHT_RECT / 2;

}
