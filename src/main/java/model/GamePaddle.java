package model;

import java.awt.Graphics;
import java.awt.Color;
import java.io.IOException;
import java.util.Objects;

/**
 * Class which represents all game paddle methods and variables.
 */
public class GamePaddle {
    /**
     * Represent game paddle coordinates on Y axis.
     */
    private static double positionY;
    private static Object gamePaddle;
    /**
     * Represent game paddle coordinates on X axis.
     */
    private int positionX;
    /**
     * Object of type Ball.
     */
    private Ball ball;
    /**
     * Represent game paddle height.
     */
    private int heightRect;
    /**
     * Represent game paddle width.
     */
    private int widthRect;
    /**
     * Variable which stands for half of HEIGHT_RECT variable.
     */
    private int halfHeightRect;
    /**
     * Constructor which initialize variables.
     */
    public GamePaddle() throws IOException {
        heightRect = Constants.HEIGHT_RECT;
        widthRect = Constants.WIDTH_RECT;
        halfHeightRect = heightRect / 2;
        double halfHeightScreen = Constants.HEIGHT_SCREEN / 2.;
        double doubleWidthRect = widthRect * 2;
        ball = new Ball();
        positionY = halfHeightScreen - doubleWidthRect;
        positionX = (int) (Constants.WIDTH_SCREEN - doubleWidthRect);
    }
    /**
     * Method with signature and body which stands for drawing game paddle.
     *
     * @param graphics object of Graphics type
     */
    public final void draw(final Graphics graphics) {
        graphics.setColor(Color.WHITE);
        graphics.fillRect(positionX, (int) positionY, widthRect, heightRect);
    }
    /**
     * Method with signature and body which stands for moving game paddle.
     */
    public final void move() {
        positionY = Ball.getY() - halfHeightRect;
        if (positionY < 0) {
            positionY = 0;
        }
        if (positionY > (Constants.HEIGHT_SCREEN - heightRect)) {
            positionY = (Constants.HEIGHT_SCREEN - heightRect);
        }
    }
    /**
     * Gag for unused method here.
     *
     * @param b param which shows whether human paddle moves
     */
    void setUpAcceleration(final boolean b) {
    }
    /**
     * Gag for unused method here.
     *
     * @param b param which shows whether human paddle moves
     */
    void setDownAcceleration(final boolean b) {
    }
    /**
     * Gag for unused method here.
     *
     * @return null
     */
    static double getX() {
        return new Double(String.valueOf(Objects.isNull(gamePaddle)));
    }
    /**
     * Getter for y coordinates of game paddle.
     *
     * @return y coordinate
     */
    static double getY() {
        return (int) positionY;
    }

    /**
     * Gag for unused method here.
     */
    void checkPaddleCords() {
    }
}
