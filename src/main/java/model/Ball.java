package model;

import java.io.IOException;
import java.awt.Graphics;
import java.awt.Color;

/**
 * Class which represents all ball methods and variables.
 */
public class Ball {
    /**
     * Represent ball speed on X axis.
     */
    private double ballSpeedX;
    /**
     * Represent ball speed on Y axis.
     */
    private double ballSpeedY;
    /**
     * Represent ball coordinates on Y axis.
     */
    private static double positionY;
    /**
     * Represent ball coordinates on X axis.
     */
    private double positionX;
    /**
     * Shows max speed number for method getRandomSpeed.
     */
    private int randomMax;
    /**
     * Object of type HumanPaddle.
     */
    private HumanPaddle humanPaddle;
    /**
     * Represents ball radius.
     */
    private int radius;
    /**
     * Variable which stands for half of RADIUS variable.
     */
    private int halfRadius;

    private int widthRect;

    private int heightRect;


    /**
     * Constructor which initialize object and variables.
     */
    public Ball() throws IOException {
        radius = Constants.RADIUS;
        halfRadius = Constants.HALF_RADIUS;
        positionY = Constants.HEIGHT_SCREEN / 2.;
        positionX = Constants.WIDTH_SCREEN / 2.;
        ballSpeedY = getRandomWay() * getRandomSpeed();
        ballSpeedX = getRandomWay() * getRandomSpeed();
        humanPaddle = new HumanPaddle();
        randomMax = Integer.valueOf(new PropertiesSetting().getPropertyValue("randomMax"));
        heightRect = Constants.HEIGHT_RECT;
        widthRect = Constants.WIDTH_RECT;

    }

    /**
     * Gets random speed.
     *
     * @return random speed
     */
    private double getRandomSpeed() {
        return (Math.random() * randomMax + 3);
    }

    /**
     * Gets random way.
     *
     * @return number which helps to choose way.
     */
    private int getRandomWay() {
        int random = (int) (Math.random() * 2);
        if (random == 1) {
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * Method with signature and body which stands for drawing ball.
     *
     * @param graphics object of Graphics type
     */
    public final void draw(final Graphics graphics) {
        graphics.setColor(Color.PINK);
        graphics.fillOval((int) positionX - halfRadius,
                (int) positionY - halfRadius, radius, radius);
    }

    /**
     * Method with signature and body
     * which check whether ball hits one of paddles.
     */
    public final void checkPaddleCords() {
        if (positionX <= ((widthRect * 2) + halfRadius)) {
            if ((positionY >= HumanPaddle.getY())
                    && (positionY <= (HumanPaddle.getY()
                    + heightRect))) {
                ballSpeedX = -ballSpeedX;
            }
        } else if (positionX >= (Constants.WIDTH_SCREEN
                - ((widthRect * 2) + halfRadius))) {
            if ((positionY >= GamePaddle.getY())
                    && (positionY <= (GamePaddle.getY()
                    + heightRect))) {
                ballSpeedX = -ballSpeedX;
            }
        }
    }

    /**
     * Method with signature and body which stands for moving ball.
     */
    public final void move() {
        positionX = positionX + ballSpeedX;
        positionY = positionY + ballSpeedY;
        if (positionY < halfRadius) {
            ballSpeedY = -ballSpeedY;
        }
        if (positionY > (Constants.HEIGHT_SCREEN - halfRadius)) {
            ballSpeedY = -ballSpeedY;
        }
    }

    /**
     * Getter for x coordinates of ball.
     *
     * @return x coordinate
     */
    public final double getX() {
        return positionX;
    }

    /**
     * Getter for y coordinates of ball.
     *
     * @return y coordinate
     */
    static double getY() {
        return positionY;
    }

    /**
     * Gag for unused method here.
     *
     * @param b param which shows whether human paddle moves
     */
    void setUpAcceleration(final boolean b) {
    }

    /**
     * Gag for unused method here.
     *
     * @param b param which shows whether human paddle moves
     */
    void setDownAcceleration(final boolean b) {
    }
}
