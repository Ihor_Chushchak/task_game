package controller;

import java.awt.Graphics;

/**
 * Interface that contains methods with only signature.
 */
public interface Controller {
    /**
     * Method stands for moving ball and paddles.
     */
    void move();

    /**
     * Method stands for drawing ball and paddles.
     *
     * @param graphics object of Graphics type
     */
    void draw(Graphics graphics);

    /**
     * Method stands for acceleration up of controlled by human paddle.
     *
     * @param b param which shows whether human paddle moves
     */
    void setUpAcceleration(boolean b);

    /**
     * Method stands for acceleration down of controlled by human paddle.
     *
     * @param b param which shows whether human paddle moves
     */
    void setDownAcceleration(boolean b);

    /**
     * Getter for x coordinate.
     *
     * @return x coordinate
     */
    double getX();

    /**
     * Check whether ball hits one of paddles.
     */
    void checkPaddleCords();


}
