package controller;

import model.Ball;
import model.GamePaddle;
import model.HumanPaddle;
import java.io.IOException;

import java.awt.Graphics;
/**
 * Class which implements all Movable interface methods.
 * Represents like main logic
 */
public class Logic implements Controller {
    /**
     * Object of type HumanPaddle.
     */
    private HumanPaddle humanPaddle;
    /**
     * Object of type GamePaddle.
     */
    private GamePaddle gamePaddle;
    /**
     * Object of type Ball.
     */
    private Ball ball;
    /**
     * Constructor which initialize objects.
     */
    public Logic() {
        try {
            humanPaddle = new HumanPaddle();
            gamePaddle = new GamePaddle();
            ball = new Ball();
        } catch (IOException e) {
            System.out.println("Problems with opening");
        } catch (ExceptionInInitializerError er) {
            System.out.println("Properties is not connected!");
        }
    }

    /**
     * Method stands for drawing ball and paddles.
     *
     * @param graphics object of Graphics type
     */
    public final void draw(final Graphics graphics) {
        humanPaddle.draw(graphics);
        gamePaddle.draw(graphics);
        ball.draw(graphics);
    }
    /**
     * Method stands for moving ball and paddles.
     */
    public final void move() {
        humanPaddle.move();
        gamePaddle.move();
        ball.move();
    }
    /**
     * Method stands for acceleration up of controlled by human paddle.
     *
     * @param b param which shows whether human paddle moves
     */
    public final void setUpAcceleration(final boolean b) {
        humanPaddle.setUpAcceleration(b);
    }
    /**
     * Method stands for acceleration down of controlled by human paddle.
     *
     * @param b param which shows whether human paddle moves
     */
    public final void setDownAcceleration(final boolean b) {
        humanPaddle.setDownAcceleration(b);
    }
    /**
     * Getter for x coordinate.
     *
     * @return x coordinate
     */
    public final double getX() {
        return (int) ball.getX();
    }
    /**
     * Check whether ball hits one of paddles.
     */
    public final void checkPaddleCords() {
        ball.checkPaddleCords();
    }

}
