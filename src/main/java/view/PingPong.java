package view;

import Exeptions.NotCreatedObjectExeption;
import controller.Logic;
import model.Constants;
import controller.Controller;
import model.PropertiesSetting;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Class which intiialize main variables,
 * starts applet viewer, draws paddles and ball.
 *
 * @author Ihor Chushchak
 * @version 1.0
 */
public class PingPong extends Applet implements Runnable, KeyListener {
    /**
     * Variable which stands for applet width viewer.
     */
    private int widthScreen ;
    /**
     * Variable which stands for applet height viewer.
     */
    private int heightScreen ;
    /**
     * Variable which stands for half HEIGHT_SCREEN variable.
     */
    private int halfHeightScreen ;
    /**
     * Variable which stands for half of WIDTH_SCREEN variable.
     */
    private int halfWidthScreen;

    private int heightRect;

    private int halfHeightRect;

    private int radius;

    private int halfRadius;
    /**
     * The only thread of the application.
     */
    private Thread thread;
    /**
     * Variable which shows whether the game has started.
     */
    private boolean gameStart;
    /**
     * Object with a type of interface Controller.
     */
    private Controller controller;

    final Logger logger = LogManager.getLogger(PropertiesSetting.class);
    /**
     * Intialize objects, variables and starts thread.
     */
    public final void init() {
        widthScreen = Constants.WIDTH_SCREEN;
        heightScreen = Constants.HEIGHT_SCREEN;
        this.resize(widthScreen, heightScreen);
        halfHeightScreen = Constants.HALF_HEIGHT_SCREEN;
        halfWidthScreen = widthScreen / 2;
        heightRect = Constants.HEIGHT_RECT;
        halfHeightRect = Constants.HALF_HEIGHT_RECT;
        radius = Constants.RADIUS;
        halfRadius = Constants.HALF_RADIUS;
        gameStart = false;
        this.addKeyListener(this);
        controller = new Logic();
        thread = new Thread(this);
        thread.start();
    }

    /**
     * Paints applet viewer, ball and paddles itself and text inside it.
     *
     * @param graphics object to draw
     */
    public final void paint(final Graphics graphics) {
        graphics.setColor(Color.BLACK);
        graphics.fillRect(0, 0, widthScreen, heightScreen);
        if ((int) controller.getX() < (-(halfRadius))
                || controller.getX() > (widthScreen + (halfRadius))) {
            graphics.setColor(Color.WHITE);
            graphics.drawString("Game is over!", halfWidthScreen
                    - radius, halfHeightScreen);
            graphics.drawString("Press ESC!", halfWidthScreen
                    - halfRadius, halfHeightScreen - radius);

        } else {
            controller.draw(graphics);
        }
        if (!gameStart) {
            graphics.setColor(Color.WHITE);
            graphics.drawString("Welcome in PingPong game!",
                    (halfWidthScreen - heightRect),
                    (halfHeightScreen - halfHeightRect - (radius * 2)));
            graphics.drawString("To move paddle press \u2191 / \u2193!",
                    (halfWidthScreen - heightRect),
                    (halfHeightScreen - halfHeightRect - radius));
            graphics.drawString("Press Enter to start!",
                    (halfWidthScreen - halfHeightRect - halfRadius),
                    (halfHeightScreen - halfHeightRect));
        }
    }

    /**
     * Updates applet viewer.
     *
     * @param graphics object to update
     */
    public final void update(final Graphics graphics) {
        paint(graphics);
    }

    /**
     * Makes ball and paddels available to move and work properly.
     */
    public final void run() {
        for (;;) {
            if (gameStart) {
                controller.move();
                controller.checkPaddleCords();
            }
            repaint();
            try {
                Thread.sleep(halfRadius);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Check which key typed.
     *
     * @param e stands for object of KeyEvent type
     */
    public final void keyTyped(final KeyEvent e) {
    }

    /**
     * If unique buttons pressed, do some accordance task.
     *
     * @param e stands for object of KeyEvent type
     */
    public final void keyPressed(final KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            controller.setUpAcceleration(true);
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            controller.setDownAcceleration(true);
        } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            gameStart = true;
        } else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            System.exit(0);
        }
    }

    /**
     * If unique buttons pressed, do some accordance task.
     *
     * @param e stands for object of KeyEvent type
     */
    public final void keyReleased(final KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            controller.setUpAcceleration(false);
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            controller.setDownAcceleration(false);
        }
    }
}
